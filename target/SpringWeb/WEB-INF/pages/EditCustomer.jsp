<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<title>Edit Customer</title>
<style>
table td{
	padding-bottom: 30px;
	padding-right: 10px;
}
</style>
</head>
<body>
	<div class="container">
        <div style="border-bottom: medium solid black;">
			<h1>Edit Customer</h1>
		</div><br>
        <form:form action="saveEdit" method="post" modelAttribute="customer">
        <div class="form-group">
        <table width="80%">
            <form:hidden path="customer_id"/>
            <tr>
                <td>FirstName:</td>
                <td width="100%"><form:input path="firstname" class="form-control"/>
                <form:errors path="Firstname" cssClass="alert-danger" />
                </td>
            </tr>
            <tr>
                <td>LastName:</td>
                <td width="100%"><form:input path="lastname" class="form-control"/>
                <form:errors path="Lastname" cssClass="alert-danger" />
                </td>
            </tr>
            <tr>
                <td>Email:</td>
                <td width="100%"><form:input path="email" class="form-control"/>
                <form:errors path="Email" cssClass="alert-danger" />
                </td>
            </tr>
            <tr>
                <td>Address:</td>
                <td width="100%"><form:textarea path="address" class="form-control" rows="4" cols="50"/>
                <form:errors path="Address" cssClass="alert-danger" />
                </td>
            </tr>
            <tr>
                <td>Telephone:</td>
                <td width="100%"><form:input path="telephone" class="form-control"/>
                <form:errors path="Telephone" cssClass="alert-danger" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="submit" value="Save" class="btn btn-success"></td>
            </tr>
        </table>
        </div>
       </div>
        </form:form>
</body>
</html>