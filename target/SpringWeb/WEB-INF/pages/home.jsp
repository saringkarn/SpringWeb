<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
<title>Customer List</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script type="text/javascript" src="webjars/jquery/2.1.1/jquery.min.js"></script>
  <script type="text/javascript" src="webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <link rel='stylesheet' href='webjars/bootstrap/3.2.0/css/bootstrap.min.css'>
</head>
<body>
	<div class="container">
	<div style="border-bottom: medium solid black;">
		<h1>Customer List</h1>
	</div><br>
	<a href="AddCustomer" ><button type="button" class="btn btn-success">Add Customer</button></a><br><br>
		<table class="table table-hover" style="table-layout: fixed; word-wrap: break-word;">

			<th>Firtname</th>
			<th>Lastname</th>
			<th>Email</th>
			<th>Telephone</th>
			<th>Address</th>
			<th style="text-align: center;">Edit/Delete</th>
			<c:forEach var="customer" items="${listCustomer}">
				<tr>

					<td>${customer.firstname}</td>
					<td>${customer.lastname}</td>
					<td>${customer.email}</td>
					<td>${customer.telephone}</td>
					<td>${customer.address}</td>
					<td style="text-align: center;">
						<a href="editCustomer?id=${customer.customer_id}"><button type="button" class="btn btn-warning">Edit</button></a>
						<a href="delCustomer?id=${customer.customer_id}"><button type="button" class="btn btn-danger">Delete</button></a>
					</td>
				
				</tr>
			</c:forEach>
		</table>
		
	</div>
</body>
</html>
