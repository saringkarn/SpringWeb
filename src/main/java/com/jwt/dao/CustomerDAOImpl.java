package com.jwt.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.Customer;



@Repository
public class CustomerDAOImpl implements CustomerDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void add(Customer customer) {
		sessionFactory.getCurrentSession().save(customer);

	}

	@SuppressWarnings("unchecked")
	public List<Customer> getAll() {
		return sessionFactory.getCurrentSession().createQuery("from Customer").list();
	}

	@Override
	public void delete(Integer employeeId) {
		Customer employee = (Customer) sessionFactory.getCurrentSession().load(
				Customer.class, employeeId);
		if (null != employee) {
			this.sessionFactory.getCurrentSession().delete(employee);
		}

	}
	@Override
	public Customer get(int empid) {
		return (Customer) sessionFactory.getCurrentSession().get(
				Customer.class, empid);
	}

	@Override
	public Customer update(Customer customer) {
		sessionFactory.getCurrentSession().update(customer);
		
		return customer;
	}

}