package com.jwt.dao;

import java.util.List;

import com.jwt.model.Customer;

public interface CustomerDAO {

	public void add(Customer customer);

	public List<Customer> getAll();

	public void delete(Integer customerId);

	public Customer update(Customer customer);

	public Customer get(int customerId);
}
