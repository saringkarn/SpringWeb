package com.jwt.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jwt.model.Customer;
import com.jwt.service.CustomerService;

@Controller
public class CustomerController {




	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/")
	public ModelAndView listEmployee(ModelAndView model) throws IOException {
		List<Customer> listcustomer = customerService.getAll();
		model.addObject("listCustomer", listcustomer);
		model.setViewName("home");
		return model;
	}

	@RequestMapping(value = "/AddCustomer")
	public ModelAndView ViewAddCustomer(ModelAndView model) {
		Customer customer = new Customer();
		model.addObject("customer", customer);
		model.setViewName("AddCustomer");
		return model;
	}
	
	@RequestMapping(value = "/saveAdd", method = RequestMethod.POST)
	public String saveAdd(@Valid Customer customer, BindingResult result) {
		if (result.hasErrors()) {
			return "AddCustomer";
		}
		customerService.add(customer);
		
		
		return "redirect:/";
	}

	@RequestMapping(value = "/saveEdit", method = RequestMethod.POST)
	public ModelAndView saveEdit(@Valid @ModelAttribute Customer customer, BindingResult result) {
		if (result.hasErrors()) {
			ModelAndView model = new ModelAndView("EditCustomer");
			model.addObject("customer", customer);
			return model;
		}
		customerService.update(customer);
		
		
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/delCustomer", method = RequestMethod.GET)
	public ModelAndView delCustomer(HttpServletRequest request) {
		int customerId = Integer.parseInt(request.getParameter("id"));
		customerService.delete(customerId);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/editCustomer", method = RequestMethod.GET)
	public ModelAndView editContact(HttpServletRequest request) {
		int employeeId = Integer.parseInt(request.getParameter("id"));
		Customer customer = customerService.get(employeeId);
		ModelAndView model = new ModelAndView("EditCustomer");
		model.addObject("customer", customer);

		return model;
	}

}