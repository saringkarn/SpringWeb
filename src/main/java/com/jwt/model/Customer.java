package com.jwt.model;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "Customer")
public class Customer implements Serializable {

	private static final long serialVersionUID = -3465813074586302847L;

	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(name = "Customer_ID")
	private int Customer_ID;
	
	@NotBlank(message="Please Input Firstname")
	private String Firstname;
	
	@NotBlank(message="Please Input Lastname")
	private String Lastname;

	@NotBlank(message="Please Input Email")
	@Email(message="Please Input Email")
	private String Email;

	@NotBlank(message="Please Input Address")
	private String Address;

	@NotBlank(message="Please Input Telephone")
	private String Telephone;

	public int getCustomer_id() {
		return Customer_ID;
	}

	public void setCustomer_id(int Customer_ID) {
		System.out.println(Customer_ID+" < ID");
		this.Customer_ID = Customer_ID;
	}

	public String getFirstname() {
		return Firstname;
	}

	public void setFirstname(String Firstname) {
		System.out.println(Firstname+" < Fname");
		this.Firstname = Firstname; 
	}

	public String getLastname() {
		return Lastname;
	}

	public void setLastname(String Lastname) {
		System.out.println(Lastname+" < Lname");
		this.Lastname = Lastname;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		System.out.println(email+" < Email");
		Email = email;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		System.out.println(address+" < Address");
		Address = address;
	}

	public String getTelephone() {
		return Telephone;
	}

	public void setTelephone(String telephone) {
		System.out.println(telephone+" < Telephone");
		Telephone = telephone;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}



}