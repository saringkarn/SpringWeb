package com.jwt.service;

import java.util.List;

import com.jwt.model.Customer;

public interface CustomerService {
	
	public void add(Customer customer);

	public List<Customer> getAll();

	public void delete(Integer customerId);

	public Customer get(int customerId);

	public Customer update(Customer customer);
}
