package com.jwt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.dao.CustomerDAO;
import com.jwt.model.Customer;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerDAO customerDAO;

	@Override
	@Transactional
	public void add(Customer customer) {
		customerDAO.add(customer);
	}

	@Override
	@Transactional
	public List<Customer> getAll() {
		return customerDAO.getAll();
	}

	@Override
	@Transactional
	public void delete(Integer customerId) {
		customerDAO.delete(customerId);
	}

	public Customer get(int customerId) {
		return customerDAO.get(customerId);
	}

	public Customer update(Customer customer) {
		// TODO Auto-generated method stub
		return customerDAO.update(customer);
	}

	public void setEmployeeDAO(CustomerDAO customerDAO) {
		this.customerDAO = customerDAO;
	}

}
